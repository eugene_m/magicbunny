//working scheme carousel init
$('.workingScheme').owlCarousel({
    loop: false,
    margin: 0,
    nav: true,
    navText: ['<i class="fa fa-angle-left" aria-hidden="true"></i>', '<i class="fa fa-angle-right" aria-hidden="true"></i>'],
    responsive: {
        0:{
            items:1
        },
        767:{
            items:3
        },
        991:{
            items:7
        }
    }
});
//end

//working scheme carousel init
$('.partnersCarousel').owlCarousel({
    loop: true,
    margin: 20,
    nav: true,
    navText: ['<i class="fa fa-angle-left" aria-hidden="true"></i>', '<i class="fa fa-angle-right" aria-hidden="true"></i>'],
    responsive: {
        0:{
            items:1
        },
        480:{
            items:2
        },
        767:{
            items:4
        },
        991:{
            items:5
        }
    }
});
//end

//related projects carousel init
$('.relatedProjectsCarousel').owlCarousel({
    loop: true,
    margin: 30,
    nav: true,
    navText: ['<i class="fa fa-angle-left" aria-hidden="true"></i>', '<i class="fa fa-angle-right" aria-hidden="true"></i>'],
    responsive: {
        0:{
            items:1
        },
        767:{
            items:2
        },
        991:{
            items:3
        }
    }
});
//end

//comment tabs height calculations

function setCommentsHeight () {
    var figures = $('.commentsTabsHolder .commentFigure');
    var figuresHolder = $('.commentsTabsHolder .tabsHolder');
    var maxHeight = Math.max.apply(null, figures.map(function () {
        return $(this).height() + 70;
    }).get());

    function setHeight (items, holder) {
        items.each(function()
        {
            $(this).css("height", maxHeight);
        });
        holder.each(function()
        {
            $(this).css("height", maxHeight);
        });
    }

    setHeight(figures, figuresHolder);
}

//end

//comment tabs height calculations init

$( document ).ready(
    setCommentsHeight()
);

window.addEventListener('resize', function(event){
    setCommentsHeight()
});



//end

//commentTabsCarousel
$(function() {
    var prev = $('.commentsSlide .tabsNav .owl-prev');
    var next = $('.commentsSlide .tabsNav .owl-next');

    prev.on('click', function(e) {
        e.preventDefault();
        var curr = $('.navTabs .active');
        curr = curr.prev();
        if(curr != null) {
            var link = curr.find('a');
            link.tab('show');
        } else {
            return false;
        }
    });

    next.on('click', function(e) {
        e.preventDefault();
        var curr = $('.navTabs .active');
        curr = curr.next();
        if(curr != null) {
            var link = curr.find('a');
            link.tab('show');
        } else {
            return false;
        }
    });
});
//

//map init

var map;
var myLatLng = {lat: 50.7440366, lng: 25.3195891};

function initMap() {
    map = new google.maps.Map(document.getElementById('map'), {
        zoom: 15,
        scrollwheel: false,
        styles: styles,
        center: myLatLng
    });

    var image = '../img/marker.png';
    var bunnyMarker = new google.maps.Marker({
        position: myLatLng,
        map: map,
        icon: image
    });
}

var styles = [
    {
        "featureType": "water",
        "elementType": "geometry.fill",
        "stylers": [
            {
                "color": "#d3d3d3"
            }
        ]
    },
    {
        "featureType": "transit",
        "stylers": [
            {
                "color": "#808080"
            },
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "road.highway",
        "elementType": "geometry.stroke",
        "stylers": [
            {
                "visibility": "on"
            },
            {
                "color": "#b3b3b3"
            }
        ]
    },
    {
        "featureType": "road.highway",
        "elementType": "geometry.fill",
        "stylers": [
            {
                "color": "#ffffff"
            }
        ]
    },
    {
        "featureType": "road.local",
        "elementType": "geometry.fill",
        "stylers": [
            {
                "visibility": "on"
            },
            {
                "color": "#ffffff"
            },
            {
                "weight": 1.8
            }
        ]
    },
    {
        "featureType": "road.local",
        "elementType": "geometry.stroke",
        "stylers": [
            {
                "color": "#d7d7d7"
            }
        ]
    },
    {
        "featureType": "poi",
        "elementType": "geometry.fill",
        "stylers": [
            {
                "visibility": "on"
            },
            {
                "color": "#ebebeb"
            }
        ]
    },
    {
        "featureType": "administrative",
        "elementType": "geometry",
        "stylers": [
            {
                "color": "#a7a7a7"
            }
        ]
    },
    {
        "featureType": "road.arterial",
        "elementType": "geometry.fill",
        "stylers": [
            {
                "color": "#ffffff"
            }
        ]
    },
    {
        "featureType": "road.arterial",
        "elementType": "geometry.fill",
        "stylers": [
            {
                "color": "#ffffff"
            }
        ]
    },
    {
        "featureType": "landscape",
        "elementType": "geometry.fill",
        "stylers": [
            {
                "visibility": "on"
            },
            {
                "color": "#efefef"
            }
        ]
    },
    {
        "featureType": "road",
        "elementType": "labels.text.fill",
        "stylers": [
            {
                "color": "#696969"
            }
        ]
    },
    {
        "featureType": "administrative",
        "elementType": "labels.text.fill",
        "stylers": [
            {
                "visibility": "on"
            },
            {
                "color": "#737373"
            }
        ]
    },
    {
        "featureType": "poi",
        "elementType": "labels.icon",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "poi",
        "elementType": "labels",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "road.arterial",
        "elementType": "geometry.stroke",
        "stylers": [
            {
                "color": "#d6d6d6"
            }
        ]
    },
    {
        "featureType": "road",
        "elementType": "labels.icon",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {},
    {
        "featureType": "poi",
        "elementType": "geometry.fill",
        "stylers": [
            {
                "color": "#dadada"
            }
        ]
    }
];

//mobile menu

function toggleMenu(trigger, menu) {
    trigger.on('click', function(e) {
        e.preventDefault();
        menu.toggleClass('closed');
    })
}

$( document ).ready(function(){
    var mobTrigger = $('#menuTrigger');
    var mobMenu = $('#menu');
    var filterTrigger = $('#filterTrigger');
    var filterMenu = $('#filter');
    toggleMenu(mobTrigger, mobMenu);
    toggleMenu(filterTrigger, filterMenu);
});

//














